require "application_system_test_case"

class ContactsTest < ApplicationSystemTestCase
  setup do
    @contact = contacts(:one)
  end

  test "visiting the index" do
    visit contacts_url
    assert_selector "h1", text: "Contacts"
  end

  test "creating a Contact" do
    visit contacts_url
    click_on "New Contact"

    fill_in "Contacts", with: @contact.contacts
    fill_in "Foo", with: @contact.foo
    fill_in "Inn", with: @contact.inn
    fill_in "Kpp", with: @contact.kpp
    fill_in "Legal Address", with: @contact.legal_address
    fill_in "Name", with: @contact.name
    fill_in "Ogrn", with: @contact.ogrn
    fill_in "Other", with: @contact.other
    fill_in "Postal Address", with: @contact.postal_address
    click_on "Create Contact"

    assert_text "Contact was successfully created"
    click_on "Back"
  end

  test "updating a Contact" do
    visit contacts_url
    click_on "Edit", match: :first

    fill_in "Contacts", with: @contact.contacts
    fill_in "Foo", with: @contact.foo
    fill_in "Inn", with: @contact.inn
    fill_in "Kpp", with: @contact.kpp
    fill_in "Legal Address", with: @contact.legal_address
    fill_in "Name", with: @contact.name
    fill_in "Ogrn", with: @contact.ogrn
    fill_in "Other", with: @contact.other
    fill_in "Postal Address", with: @contact.postal_address
    click_on "Update Contact"

    assert_text "Contact was successfully updated"
    click_on "Back"
  end

  test "destroying a Contact" do
    visit contacts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Contact was successfully destroyed"
  end
end
