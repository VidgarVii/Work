require "application_system_test_case"

class TruckingindustriesTest < ApplicationSystemTestCase
  setup do
    @truckingindustry = truckingindustries(:one)
  end

  test "visiting the index" do
    visit truckingindustries_url
    assert_selector "h1", text: "Truckingindustries"
  end

  test "creating a Truckingindustry" do
    visit truckingindustries_url
    click_on "New Truckingindustry"

    fill_in "Autocarrier", with: @truckingindustry.autocarrier
    fill_in "Buyer", with: @truckingindustry.buyer
    fill_in "Coastofroad", with: @truckingindustry.coastofroad
    fill_in "Numberauto", with: @truckingindustry.numberauto
    fill_in "Order", with: @truckingindustry.order
    fill_in "Price Buy", with: @truckingindustry.price-buy
    fill_in "Price Sale", with: @truckingindustry.price-sale
    fill_in "Priceofroad", with: @truckingindustry.priceofroad
    fill_in "Supplier", with: @truckingindustry.supplier
    click_on "Create Truckingindustry"

    assert_text "Truckingindustry was successfully created"
    click_on "Back"
  end

  test "updating a Truckingindustry" do
    visit truckingindustries_url
    click_on "Edit", match: :first

    fill_in "Autocarrier", with: @truckingindustry.autocarrier
    fill_in "Buyer", with: @truckingindustry.buyer
    fill_in "Coastofroad", with: @truckingindustry.coastofroad
    fill_in "Numberauto", with: @truckingindustry.numberauto
    fill_in "Order", with: @truckingindustry.order
    fill_in "Price Buy", with: @truckingindustry.price-buy
    fill_in "Price Sale", with: @truckingindustry.price-sale
    fill_in "Priceofroad", with: @truckingindustry.priceofroad
    fill_in "Supplier", with: @truckingindustry.supplier
    click_on "Update Truckingindustry"

    assert_text "Truckingindustry was successfully updated"
    click_on "Back"
  end

  test "destroying a Truckingindustry" do
    visit truckingindustries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Truckingindustry was successfully destroyed"
  end
end
