require 'test_helper'

class TruckingindustriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @truckingindustry = truckingindustries(:one)
  end

  test "should get index" do
    get truckingindustries_url
    assert_response :success
  end

  test "should get new" do
    get new_truckingindustry_url
    assert_response :success
  end

  test "should create truckingindustry" do
    assert_difference('Truckingindustry.count') do
      post truckingindustries_url, params: { truckingindustry: { autocarrier: @truckingindustry.autocarrier, buyer: @truckingindustry.buyer, coastofroad: @truckingindustry.coastofroad, numberauto: @truckingindustry.numberauto, order: @truckingindustry.order, price-buy: @truckingindustry.price-buy, price-sale: @truckingindustry.price-sale, priceofroad: @truckingindustry.priceofroad, supplier: @truckingindustry.supplier } }
    end

    assert_redirected_to truckingindustry_url(Truckingindustry.last)
  end

  test "should show truckingindustry" do
    get truckingindustry_url(@truckingindustry)
    assert_response :success
  end

  test "should get edit" do
    get edit_truckingindustry_url(@truckingindustry)
    assert_response :success
  end

  test "should update truckingindustry" do
    patch truckingindustry_url(@truckingindustry), params: { truckingindustry: { autocarrier: @truckingindustry.autocarrier, buyer: @truckingindustry.buyer, coastofroad: @truckingindustry.coastofroad, numberauto: @truckingindustry.numberauto, order: @truckingindustry.order, price-buy: @truckingindustry.price-buy, price-sale: @truckingindustry.price-sale, priceofroad: @truckingindustry.priceofroad, supplier: @truckingindustry.supplier } }
    assert_redirected_to truckingindustry_url(@truckingindustry)
  end

  test "should destroy truckingindustry" do
    assert_difference('Truckingindustry.count', -1) do
      delete truckingindustry_url(@truckingindustry)
    end

    assert_redirected_to truckingindustries_url
  end
end
