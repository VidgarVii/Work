class AddUserRefToSuppliers < ActiveRecord::Migration[5.2]
  def change
    add_reference :suppliers, :user, foreign_key: true
  end
end
