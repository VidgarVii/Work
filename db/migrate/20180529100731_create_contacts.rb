class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :foo
      t.string :name
      t.integer :inn 
      t.integer :kpp
      t.integer :ogrn
      t.string :legal_address
      t.string :postal_address
      t.string :contacts
      t.string :other

      t.timestamps
    end
  end
end
