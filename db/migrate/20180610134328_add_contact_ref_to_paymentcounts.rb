class AddContactRefToPaymentcounts < ActiveRecord::Migration[5.2]
  def change
    add_reference :paymentcounts, :contact, foreign_key: true
  end
end
