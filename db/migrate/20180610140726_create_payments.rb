class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :number, :limit => 20
      t.belongs_to :contact
      t.belongs_to :bank
      

      t.timestamps
    end

    drop_table :paymentcounts
  end
end
