class CreateBanks < ActiveRecord::Migration[5.2]
  def change
    create_table     :banks do |t|
    	t.string     :name
    	t.integer	 :bik
    	t.integer	 :kpp


      t.timestamps
    end
  end
end
