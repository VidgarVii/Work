class AddColumnsToSuppliers < ActiveRecord::Migration[5.2]
  def change
  	change_table :suppliers do |t|
          t.integer :phone
          t.string :address
          t.string :payment_account
     end
  end
end
