class AddPaymentAccountRefToContact < ActiveRecord::Migration[5.2]
  def change
    add_reference :contacts, :payment_account, foreign_key: true
  end
end
