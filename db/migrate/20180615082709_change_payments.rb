class ChangePayments < ActiveRecord::Migration[5.2]
    def change
      add_column :payments, :bik, :integer, :limit => 9
      add_column :payments, :corr, :integer, :limit => 20
      add_column :payments, :bank, :string, :limit => 50

      add_index :payments, :number,                unique: true
      add_index :payments, :bik               
      #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
