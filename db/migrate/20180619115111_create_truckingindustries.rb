class CreateTruckingindustries < ActiveRecord::Migration[5.2]
  def change
    create_table :truckingindustries do |t|
      t.string :order, :limit => 20
      t.integer :price_buy, :limit => 6
      t.integer :price_sale, :limit => 6
      t.string :supplier, :limit => 20
      t.string :buyer, :limit => 20
      t.string :autocarrier, :limit => 20
      t.string :numberauto, :limit => 10
      t.integer :priceofroad
      t.integer :coastofroad

      t.timestamps
    end
    add_index :truckingindustries, :order
    add_index :truckingindustries, :supplier
    add_index :truckingindustries, :buyer
    add_index :truckingindustries, :autocarrier
  end
end
