class RemoveColumnToContact < ActiveRecord::Migration[5.2]
  def change
    remove_column :contacts, :payment_id
  end
end
