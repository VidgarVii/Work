# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_06_20_073942) do

  create_table "banks", force: :cascade do |t|
    t.string "name"
    t.integer "bik"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cor_acc", limit: 20
  end

  create_table "contacts", force: :cascade do |t|
    t.string "foo"
    t.string "name"
    t.integer "inn"
    t.integer "kpp"
    t.integer "ogrn"
    t.string "legal_address"
    t.string "postal_address"
    t.string "contacts"
    t.string "other"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "payments", force: :cascade do |t|
    t.integer "number", limit: 20
    t.integer "contact_id"
    t.integer "bank_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "bik", limit: 9
    t.integer "corr", limit: 20
    t.string "bank", limit: 50
    t.index ["bank_id"], name: "index_payments_on_bank_id"
    t.index ["bik"], name: "index_payments_on_bik"
    t.index ["contact_id"], name: "index_payments_on_contact_id"
    t.index ["number"], name: "index_payments_on_number", unique: true
  end

  create_table "truckingindustries", force: :cascade do |t|
    t.string "order", limit: 20
    t.integer "price_buy", limit: 6
    t.integer "price_sale", limit: 6
    t.string "supplier", limit: 20
    t.string "buyer", limit: 20
    t.string "autocarrier", limit: 20
    t.string "numberauto", limit: 10
    t.integer "priceofroad"
    t.integer "coastofroad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "add_weight", limit: 6
    t.integer "rm_weight", limit: 6
    t.index ["autocarrier"], name: "index_truckingindustries_on_autocarrier"
    t.index ["buyer"], name: "index_truckingindustries_on_buyer"
    t.index ["order"], name: "index_truckingindustries_on_order"
    t.index ["supplier"], name: "index_truckingindustries_on_supplier"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
