json.extract! contact, :id, :foo, :name, :inn, :kpp, :ogrn, :legal_address, :postal_address, :contacts, :other, :created_at, :updated_at
json.url contact_url(contact, format: :json)
