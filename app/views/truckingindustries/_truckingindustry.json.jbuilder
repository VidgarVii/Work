json.extract! truckingindustry, :id, :order, :price_buy, :price_sale, :supplier, :buyer, :autocarrier, :numberauto, :priceofroad, :coastofroad, :created_at, :updated_at
json.url truckingindustry_url(truckingindustry, format: :json)
