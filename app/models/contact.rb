class Contact < ApplicationRecord

  validates  :inn, presence: true
  validates  :name, presence: true
  validates  :foo, presence: true

  belongs_to :user
  has_many   :payments, dependent: :destroy, validate: true
  validates_associated :payments
  
 
end
