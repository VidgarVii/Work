class Payment < ApplicationRecord
    
   validates  :number, presence: true
   validates  :number, length: { in: 1..20 }
   validates  :corr, length: {maximum: 20 }
   validates  :bik, length: {maximum:9}
   validates  :bank, length: {maximum: 50 }
    
   belongs_to  :contact, optional: true
    
end
