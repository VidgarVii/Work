class PaymentsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
   
   def index
      @payment = Payment.where('contact_id = ?', params[:contact_id])
      @contact = Contact.find(params[:contact_id])
   end

    def show
        @contact = Contact.find(params[:contact_id])
        @payment = @contact.payments.find(params[:id])
    end
    
   def error
   end

   def new
       @contact = Contact.find(params[:contact_id])
   end
  
    def create
        
        @contact = Contact.find(params[:contact_id])
        @payment = @contact.payments.new(params_pay)
        
            if @payment.save
              redirect_to contact_path(@contact), notice: 'Расчетный счет сохранен.'
            else
              @contact.valid?                       
              render :error                      
            end
    end


    # GET /contacts/1/edit
    def edit
    end

    def update
        @contact = Contact.find(params[:contact_id])
        @payment = @contact.payments.find(params[:id])
        #@payment.update(params_pay)
        
        if @payment.update(params_pay)
            redirect_to contact_path(@contact)
        end

    end

    def destroy
        @contact = Contact.find(params[:contact_id])
        @payment = @contact.payments.find(params[:id])
        @payment.destroy
        redirect_to contact_path(@contact)
    end

    private

    def set_payment
        @contact = Contact.find(params[:contact_id])
        @payment = Payment.find(params[:id])
    end


    def params_pay
        params.require(:payment).permit(:number, :bik, :corr, :bank)
    end

end
