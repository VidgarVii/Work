class TruckingindustriesController < ApplicationController
  before_action :set_truckingindustry, only: [:show, :edit, :update, :destroy]

  # GET /truckingindustries
  # GET /truckingindustries.json
  def index
    @truckingindustries = Truckingindustry.all
  end

  # GET /truckingindustries/1
  # GET /truckingindustries/1.json
  def show
  end

  # GET /truckingindustries/new
  def new
    @truckingindustry = Truckingindustry.new
  end

  # GET /truckingindustries/1/edit
  def edit
  end

  # POST /truckingindustries
  # POST /truckingindustries.json
  def create
    @truckingindustry = Truckingindustry.new(truckingindustry_params)
    
    @truckingindustry.coastofroad = (truckingindustry_params['priceofroad'].to_i * truckingindustry_params['rm_weight'].to_i)/1000

    if @truckingindustry.add_weight=""
      @truckingindustry.add_weight=truckingindustry_params['rm_weight']
    end
    
    respond_to do |format|
      if @truckingindustry.save
        format.html { redirect_to @truckingindustry, notice: 'Truckingindustry was successfully created.' }
        format.json { render :show, status: :created, location: @truckingindustry }
      else
        format.html { render :new }
        format.json { render json: @truckingindustry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /truckingindustries/1
  # PATCH/PUT /truckingindustries/1.json
  def update
    respond_to do |format|
      if @truckingindustry.update(truckingindustry_params)
        format.html { redirect_to @truckingindustry, notice: 'Truckingindustry was successfully updated.' }
        format.json { render :show, status: :ok, location: @truckingindustry }
      else
        format.html { render :edit }
        format.json { render json: @truckingindustry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /truckingindustries/1
  # DELETE /truckingindustries/1.json
  def destroy
    @truckingindustry.destroy
    respond_to do |format|
      format.html { redirect_to truckingindustries_url, notice: 'Truckingindustry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_truckingindustry
      @truckingindustry = Truckingindustry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def truckingindustry_params
      params.require(:truckingindustry).permit(:order, :price_buy, :price_sale, :add_weight, :rm_weight, :supplier, :buyer, :autocarrier, :numberauto, :priceofroad, :coastofroad)
    end
end
