Rails.application.routes.draw do

  resources :banks
  resources :truckingindustries
  
  resources :contacts do
    resources :payments
  end


  devise_for :users

  root 'pages#index'

  get 'office(/:id)', to: 'pages#office'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
